import { GraphQLList } from "graphql";
import { Products } from "../../Entities/Product";
import { ProductType } from "../typesDefs/Product";

export const GET_ALL_PRODUCTS = {
   type: new GraphQLList (ProductType),
    async resolve(){
        const result = await Products.find();
        return result;
    }
}
