import {GraphQLSchema,GraphQLObjectType} from 'graphql';
import { CREATE_PRODUCT } from './Mutations/Product';
import { GREETING } from './Queries/Greeting';
import { GET_ALL_PRODUCTS } from './Queries/Product';

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        greeting: GREETING,
        getAllProducts: GET_ALL_PRODUCTS
    }
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createProduct: CREATE_PRODUCT
    }
})

export const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})